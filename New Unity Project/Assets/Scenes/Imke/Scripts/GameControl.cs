﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;



public class GameControl : MonoBehaviour
{
   
    /// <Credits>
    /// Ronan heeft me geholpen met de collision (is position free)
    /// </summary>
    [Header("Player/Mole attributes")]
    [SerializeField] private Player[] obj_Player;
    [SerializeField] private GameObject[] obj_PlayerStartPos;

    [Header("Hamer attributes")]
    [SerializeField] private Hamer obj_Hamer;
    [SerializeField] private GameObject obj_HamerStartPos;
    [SerializeField] private Animator m_Animator;

    //IAnimationCheck m_AnimationCheck;


    private void Start()                                                                       // Move the players to the start Position in the beginning
    {
        for (int i = 0; i < obj_Player.Length; i++)
        {
            obj_Player[i].transform.position = obj_PlayerStartPos[i].transform.position;
        }
        obj_Hamer.transform.position = obj_HamerStartPos.transform.position;
    }
    public bool IsPositionFree(int playerNumber, int newPos)                                   // check if player can go to ...position, if not let player stay in the same pos.
    {
        for (int i = 0; i < obj_Player.Length; i++)
        {
            
            if (newPos != obj_Player[i].m_HolePos && obj_Player[i].m_PlayerNumber != playerNumber)
            {
                
                return true;
            }
        }
       
        return false;
    }
    //void CanPlayHamerAnimation()
    //{
    //    if (m_AnimationCheck.HamerMovement() == true)
    //    {
            
    //    }
    //}
    private void ResetAndQuit()                                                                // Easy reset and quit button for prototype
    {
        if (Input.GetKeyDown(KeyCode.Backspace) || Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(1);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(0);
        }
    }

}
