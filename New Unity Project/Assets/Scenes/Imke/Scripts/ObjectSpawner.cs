﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObjectSpawner : MonoBehaviour {

    [SerializeField] GameObject[] obj_SpawnObject;
    [SerializeField] Transform[] m_HolePos;
   
    private Transform m_RandomSelectedHolePos;

    int m_IndexHolePos;
    int m_IndexObject;

    public float m_Timer = 5f;
    bool m_objHasSpawned = false;

    private void Start()
    {
        for (int i = 0; i < obj_SpawnObject.Length; i++)
        {
            obj_SpawnObject[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update() {
        m_Timer -= Time.deltaTime;
        if(m_Timer <= 0)
        {
            RandomPos();
            SpawnObject();
            m_Timer = 4.5f;
            m_objHasSpawned = true;
            
        }
        SetObjectActiveToFalse();
       // OnTriggerEnter();

    }

    private void SpawnObject()
    {
        m_IndexObject = Random.Range(0, obj_SpawnObject.Length);
        obj_SpawnObject[m_IndexObject].SetActive(true);
        
    }

    private void RandomPos()
    {
        m_IndexHolePos = Random.Range(0, m_HolePos.Length);
        m_RandomSelectedHolePos = m_HolePos[m_IndexHolePos];
    }

    private void SetObjectActiveToFalse()
    {
        if (m_Timer <= 0.4f)
        {
            obj_SpawnObject[m_IndexObject].SetActive(false);
            m_objHasSpawned = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            obj_SpawnObject[m_IndexObject].SetActive(false);
            m_objHasSpawned = false;
            // add points to scoreboard
            Debug.Log("Hit coin, Added Points");
        }
    }

}
