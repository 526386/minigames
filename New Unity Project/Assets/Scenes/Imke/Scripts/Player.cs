﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameControl m_GameControl    ;
    public int m_PlayerNumber;
    public int m_HolePos;

    [SerializeField] private Transform m_PlayerItselfPos;
    [SerializeField] private KeyCode[] m_Keys;
    [SerializeField] private Transform[] m_BoardPos;

    [SerializeField] private PlayerState2 m_StateOfPlayer;

    private float f_ButtonCoundDown = 0f;
    private float f_TimerButton = 0.8f;

     private void Start()
    {
        m_StateOfPlayer.GetComponent<PlayerState2>();
    }
    private void Update()
    {
        f_ButtonCoundDown -= Time.deltaTime;
        transform.position = m_PlayerItselfPos.transform.position;
        MoleButtonMovement();

    }
  private  void MoleButtonMovement()
    {
        if (f_ButtonCoundDown <= 0)
        {
            for (int i = 0; i < m_Keys.Length; i++)
            {
                if (Input.GetKeyDown(m_Keys[i]))
                {
                    
                    if (m_GameControl.IsPositionFree(m_PlayerNumber, i) == true)
                    {
                        m_PlayerItselfPos.transform.position = m_BoardPos[i].position;
                        f_ButtonCoundDown = f_TimerButton;
                        m_HolePos = i;
                        m_StateOfPlayer.m_State = StateOfPlayer.PoppingOut;
                    }
                }
            }
        }
  }
}
