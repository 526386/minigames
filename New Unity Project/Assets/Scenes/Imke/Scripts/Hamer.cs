﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
//public interface IAnimationCheck
//{
//    bool HamerMovement();
//}

public class Hamer : MonoBehaviour /*IAnimationCheck*/
{
    /// <Credits>
    /// Bram heeft me geholpen met de left and right trigger van de xbox controller omdat die op axisses werken.
    /// </summary>
    
    [SerializeField] private KeyCode[] m_Keys;
    [SerializeField] Transform[] m_BoardPos;

    private bool b_previousTrigger1Down = false;
    private bool b_previousTrigger2Down = false;


    private float f_ButtonCoundDown = 0f;
    private float f_TimerButton = 0.6f;
    [SerializeField] private Animator m_Animator;

    //IAnimationCheck m_AnimationCheck;



    void Update()
    {
        f_ButtonCoundDown -= Time.deltaTime;
        HamerMovement();
    }

    void  HamerMovement()
    {
        bool trigger1Pressed = false;
        bool trigger1IsCurrentlyDown = (Input.GetAxis("LeftTrigger") > 0.5f);
        if (trigger1IsCurrentlyDown && !b_previousTrigger1Down) trigger1Pressed = true;
        b_previousTrigger1Down = trigger1IsCurrentlyDown;

        bool trigger2Pressed = false;
        bool trigger2IsCurrentlyDown = (Input.GetAxis("RightTrigger") > 0.5f);
        if (trigger2IsCurrentlyDown && !b_previousTrigger2Down) trigger2Pressed = true;
        b_previousTrigger2Down = trigger2IsCurrentlyDown;

        if (f_ButtonCoundDown <= 0 )
        {
            
            for (int i = 0; i < m_Keys.Length; i++)
            {
                if (Input.GetKeyDown(m_Keys[1]) || trigger1Pressed)
                {
                    transform.position = m_BoardPos[1].position;
                    f_ButtonCoundDown = f_TimerButton;
                    
                    m_Animator.Play("Hamer_Animation");
                }
                else if(Input.GetKeyDown(m_Keys[3]) || trigger2Pressed)
                {
                    transform.position = m_BoardPos[3].position;
                    f_ButtonCoundDown = f_TimerButton;
                  
                    m_Animator.Play("Hamer_Animation");
                }
                else if(Input.GetKeyDown(m_Keys[i]))
                {
                    transform.position = m_BoardPos[i].position;
                    f_ButtonCoundDown = f_TimerButton;
                   
                    m_Animator.Play("Hamer_Animation");

                }
                
                
            }
            
            
          
        }
       
        
        


    }
}
