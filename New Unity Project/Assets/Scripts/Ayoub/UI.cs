﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UI : MonoBehaviour {

    [SerializeField]
    private PlayerState2 m_Player1;

    [SerializeField]
    private PlayerState2 m_Player2;

    [SerializeField]
    private PlayerState2 m_Player3;

    [SerializeField]
    private int m_MaxCoinCounter;

    public int m_CurCoinCounter;

    [SerializeField]
    private TextMeshProUGUI m_TeamCoinCounter;

    [SerializeField]
    private TextMeshProUGUI m_EndGameText;

    private void Start()
    {
        m_EndGameText.text = "";
    }

    void Update ()
    {
        WinController();

        m_TeamCoinCounter.text = "Coins: " + m_CurCoinCounter + "/" + m_MaxCoinCounter;
    }

    private void WinController()
    {
        if (m_Player1.m_State == StateOfPlayer.Stunned && m_Player2.m_State == StateOfPlayer.Stunned && m_Player3.m_State == StateOfPlayer.Stunned && m_CurCoinCounter < m_MaxCoinCounter)
        {
            HammerWins();
        }
        else if (m_CurCoinCounter >= m_MaxCoinCounter)
        {
            Playerswin();

            m_CurCoinCounter = m_MaxCoinCounter;
        }
    }

    private void HammerWins()
    {
        m_EndGameText.text = "Hammer wins!";
    }

    private void Playerswin()
    {
        m_EndGameText.text = "Players win!";
    }
}
