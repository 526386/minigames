﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerState2 : MonoBehaviour {

    [SerializeField]
    private float m_MaxStunnedTimer;

    [SerializeField]
    private float m_MaxPoppingTimer;

    private float m_Timer;

    public StateOfPlayer m_State;

    [SerializeField]
    private ParticleSystem m_GotHitEffect;

    [SerializeField]
    private AudioSource m_AudioSource;

    [SerializeField]
    private AudioClip m_PainSound;

    private Vector3 m_CurrentPosition;

    [SerializeField]
    private UI m_UI;

    [SerializeField]
    private Image m_PlayerNeutralIcon;

    private void Start()
    {
        m_Timer = 0f;

        m_AudioSource.clip = m_PainSound;
    }

    void Update ()
    {
        switch (m_State)
        {
            case (StateOfPlayer.Neutral):

                PlayerIsNeutral();

                break;

            case (StateOfPlayer.Stunned):

                PlayerIsStunned();

                break;

            case (StateOfPlayer.PoppingOut):

                PlayerIsPoppingOut();

                break;
        }

        TimeChecker();
    }

    private void PlayerIsNeutral()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            m_State = StateOfPlayer.PoppingOut;
        }

        m_PlayerNeutralIcon.gameObject.SetActive(true);
    }

    private void PlayerIsPoppingOut()
    {
        if (m_Timer <= 0 && m_State != StateOfPlayer.Neutral)
        {
            PoppingOutCounter(m_MaxPoppingTimer);
        }
    }

    private void PlayerIsStunned()
    {
        m_PlayerNeutralIcon.gameObject.SetActive(false);

        if (m_Timer <= 0)
        {
            PoppingOutCounter(m_MaxStunnedTimer);
        }
    }

    private void PoppingOutCounter(float MaxTimer)
    {
        m_Timer = m_Timer + MaxTimer;
    }

    private void TimeChecker()
    {
        if (m_Timer > 0 && m_State != StateOfPlayer.Neutral)
        {
            m_Timer -= Time.deltaTime;
        }

        if (m_Timer < 0)
        {
            m_Timer = 0;

            m_State = StateOfPlayer.Neutral;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Hammer")
        {
            if (m_State == StateOfPlayer.PoppingOut)
            {
                m_State = StateOfPlayer.Stunned;

                m_GotHitEffect.Play();

                m_AudioSource.Play();

                m_Timer = 0;
            }
        }

        if(other.tag == "Coin")
        {
            if (m_State == StateOfPlayer.PoppingOut)
            {
                m_UI.m_CurCoinCounter += 1;
            }
        }
    }
}