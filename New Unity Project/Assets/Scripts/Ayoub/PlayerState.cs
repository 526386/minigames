﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateOfPlayer
{
    Neutral,
    Stunned,
    PoppingOut
}

// Checks what happens when the Player is stunned or not
public class PlayerState : MonoBehaviour
{
    [SerializeField]
    private int m_MaxStunnedTimer;

    [SerializeField]
    private float m_MaxPoppingTimer;

    private Vector3 m_CurrentPosition;

    public StateOfPlayer m_State;

    [SerializeField]
    private AudioSource m_PainSound;

    private bool m_Stunned = false;

    //[SerializeField]
    //private Score m_Score;

    //[SerializeField]
    //private float m_CurTimer;

    //private float m_MaxTimer;

    void Start()
    {
        m_CurrentPosition = this.transform.position;

        //m_State = StateOfPlayer.Neutral;

        //m_CurTimer = m_MaxTimer;
    }

    void Update()
    {
        switch (m_State)
        {
            case (StateOfPlayer.Neutral):

                PlayerIsNeutral();

                break;
            case (StateOfPlayer.Stunned):

                PlayerIsPoppingOut();
                
                break;

            case (StateOfPlayer.PoppingOut):

                PlayerIsPoppingOut();
                
                //countdown = m_MaxTimer;

                break;
        }

        Debug.Log(m_State);
    }

    // When the player is NOT stunned
    private void PlayerIsNeutral()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_State = StateOfPlayer.PoppingOut;
        }
    }

    // When the player is stunned
    private void PlayerIsStunned()
    {
        StartCoroutine("StunnedTime");
    }

    // When the player pops out
    private void PlayerIsPoppingOut()
    {
        StartCoroutine("PoppingTime");
    }

    IEnumerator StunnedTime()
    {
        yield return new WaitForSeconds(m_MaxStunnedTimer);

        m_State = StateOfPlayer.Neutral;
    }

    IEnumerator PoppingTime()
    {
        yield return new WaitForSeconds(m_MaxPoppingTimer);

        m_State = StateOfPlayer.Neutral;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hammer" && m_State == StateOfPlayer.PoppingOut)
        {
            m_PainSound.Play();

            this.m_State = StateOfPlayer.Stunned;

            //m_Score.AddTeam1Score(10);
        }
    }

    //private void Timer(float timer)
    //{
    //    float countdown = timer;

    //    if(countdown > 0)
    //    {
    //        countdown -= Time.time;
    //    }

    //    if(countdown < 0)
    //    {
    //        m_State = StateOfPlayer.Neutral;

    //        countdown = 2;
    //    }

    //    Debug.Log(countdown);
    //}

    private void ResetTimer()
    {
        
    }
}
