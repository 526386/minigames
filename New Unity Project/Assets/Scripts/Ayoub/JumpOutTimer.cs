﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// De Timer heb ik gemaakt met behulp van https://www.youtube.com/watch?v=RLKQhpDxDlE
public class JumpOutTimer : MonoBehaviour {

    [SerializeField]
    private int m_MaxPopTimer;
    private int m_CurPopTimer;

    [SerializeField]
    private GameObject m_Object;

    [SerializeField]
    private PlayerState m_Player;

	// Use this for initialization
	void Start ()
    {
        m_CurPopTimer = m_MaxPopTimer;
	}
	
	// Update is called once per frame
	void Update ()
    {

        if(m_Player.m_State == StateOfPlayer.PoppingOut)
        {
            m_CurPopTimer = m_MaxPopTimer;

            StartCoroutine("PoppingTime");
        }

        if(m_Player.m_State == StateOfPlayer.Stunned)
        {
            m_Object.SetActive(true);
        }
	}

    IEnumerator PoppingTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            m_CurPopTimer--;
        }
    }
}
